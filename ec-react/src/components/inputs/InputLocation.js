import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {ErrorMessage} from 'formik';

import {locations} from '../../core';
import Select from './Select';
import s from './input.module.scss';

const ufList = [
  'AC',
  'AL',
  'AM',
  'AP',
  'BA',
  'CE',
  'DF',
  'ES',
  'GO',
  'MA',
  'MG',
  'MS',
  'MT',
  'PA',
  'PB',
  'PE',
  'PI',
  'PR',
  'RJ',
  'RN',
  'RO',
  'RR',
  'RS',
  'SC',
  'SE',
  'SP',
  'TO',
];

async function reloadCities({uf, names, setState, setFieldValue}) {
  setState({cityList: [], loadingCities: true});

  try {
    const cityList = (await locations('BR', uf)).map((e) => e.name);
    setState({cityList, loadingCities: false});
  } catch (err) {
    console.error(err);

    setState({cityList: [], loadingCities: false});

    setFieldValue(names[0], '');
    setFieldValue(names[1], '');
  }
}

const InputLocation = ({
  names,
  values,
  setFieldValue,
  onChange,
  onBlur,
  className,
  width,
  loaderColor,
  firstUf,
  ufSelectProps,
  citySelectProps,
  cityPlaceholder = 'Selecione uma cidade',
  ufPlaceholder = 'Estado',
}) => {
  const [state, setState] = useState({
    cityList: [],
    loadingCities: false,
  });

  const uf = values[names[0]];
  useEffect(() => {
    uf && reloadCities({uf, names, setState, setFieldValue});
  }, [uf]);

  const props = (n) => ({
    name: names[n],
    value: values[names[n]],
    className,
    onBlur,
  });
  return (
    <div
      style={{
        display: 'inline-block',
        position: 'relative',
        width,
      }}
    >
      <Select
        options={firstUf ? [firstUf, ...ufList] : ufList}
        placeholder={ufPlaceholder}
        style={{width: '25%'}}
        {...props(0)}
        onChange={(...args) => {
          setFieldValue(names[1], '');
          onChange(...args);
        }}
        {...ufSelectProps}
      />
      <Select
        loaderColor={loaderColor}
        loading={state.loadingCities}
        options={state.cityList}
        placeholder={cityPlaceholder}
        style={{marginLeft: '1%', width: '74%'}}
        onChange={onChange}
        {...props(1)}
        {...citySelectProps}
      />
      <ErrorMessage name={names[1]} component="div" className={s.error} />
    </div>
  );
};

InputLocation.defaultProps = {
  className: '',
  loaderColor: '#777',
  width: '100%',
};

InputLocation.propTypes = {
  className: PropTypes.string,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  loaderColor: PropTypes.string,
  names: PropTypes.arrayOf(PropTypes.string),
  setFieldValue: PropTypes.func.isRequired,
  values: PropTypes.object,
  width: PropTypes.string,
  firstUf: PropTypes.string,
  ufSelectProps: PropTypes.object,
  citySelectProps: PropTypes.object,
};

export default InputLocation;
