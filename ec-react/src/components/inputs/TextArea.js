import React from 'react';
import PropTypes from 'prop-types';
import {ErrorMessage} from 'formik';

import s from './input.module.scss';

const TextArea = React.forwardRef(
  ({className, ...props}, ref) => {
    let element = <textarea className={className} ref={ref} {...props} />;

    return (
      <div
        style={{
          position: 'relative',
        }}
      >
        {element}
        <ErrorMessage name={props.name} component="div" className={s.error} />
      </div>
    );
  }
);

TextArea.defaultProps = {
  className: '',
};

TextArea.propTypes = {
  className: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
};

export default TextArea;
