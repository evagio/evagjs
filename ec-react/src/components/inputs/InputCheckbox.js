import React from 'react';
import PropTypes from 'prop-types';

import s from './input.module.scss';

const InputCheckbox = ({className, label, ...props}) => (
  <label className={className}>
    <input type="checkbox" {...props} />
    <span>{label}</span>
  </label>
);

InputCheckbox.defaultProps = {
  className: '',
  label: '',
};

InputCheckbox.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
};

export default InputCheckbox;
