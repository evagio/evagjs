import React from 'react';
import PropTypes from 'prop-types';
import {TailSpin} from 'react-loader-spinner';

import s from './input.module.scss';

const Select = ({
  className,
  loaderColor,
  loading,
  options,
  placeholder,
  ...props
}) => {
  if (loading) {
    return (
      <span className={className + ` ${s.loading}`} style={props.style}>
        <span className={s.spinner}>
          <TailSpin color={loaderColor} height={13} width={13} />
        </span>
        Carregando...
      </span>
    );
  }

  return (
    <select
      className={className + (props.value === '' ? ` ${s.unset}` : '')}
      {...props}
    >
      <option disabled={true} value="">
        {placeholder}
      </option>
      {options.map((opt, i) => {
        if (typeof opt === 'string') opt = {value: opt, label: opt};
        return (
          <option key={i} value={opt.value}>
            {opt.label}
          </option>
        );
      })}
    </select>
  );
};

Select.defaultProps = {
  className: '',
  loading: false,
  options: [],
  style: {},
  value: '',
};

Select.propTypes = {
  className: PropTypes.string,
  loaderColor: PropTypes.string,
  loading: PropTypes.bool,
  options: PropTypes.arrayOf(PropTypes.string),
  placeholder: PropTypes.string,
  style: PropTypes.object,
  value: PropTypes.string,
};

export default Select;
