import React from 'react';
import PropTypes from 'prop-types';

import s from './ModalError.module.scss';

const ModalError = ({error, dismiss}) =>
  error ? (
    <div className={s.overlay} onClick={dismiss}>
      <div className={s.modal} onClick={(e) => e.stopPropagation()}>
        <p>{error}</p>
      </div>
    </div>
  ) : null;

ModalError.propTypes = {
  dismiss: PropTypes.func,
  error: PropTypes.string,
};

// TODO: Animate entrance.
// TODO: Add button to dismiss.

export default ModalError;
