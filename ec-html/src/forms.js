import Cleave from 'cleave.js';

import {captchaV2Key, cepData, locations, submit, logErr} from './core';
import {displayMessage} from './snackbar';

import recaptcha from './recaptcha';
import {
  trackConversion,
} from '../../core/tracking';

export const clearForm = function (form) {
  form
    .querySelectorAll(
      `input[type="text"], input[type="email"],
      input[type="tel"], select, textarea`
    )
    .forEach((input) => {
      input.value = '';
    });

  form
    .querySelectorAll(
      `input[type="radio"],
      input[type="checkbox"]`
    )
    .forEach((input) => {
      input.checked = false;
    });
};

const jump = function (form, jumpSteps) {
  const multiforms = form.closest('[data-evag="multiforms"]');
  if (!multiforms) {
    throw new Error('could not jump with no multiforms parent');
  }

  const forms = multiforms.querySelectorAll('[data-evag="form"]');

  let cur = -1;
  for (let i = 0; i < forms.length; i++) {
    if (form === forms[i]) {
      cur = i;
    }
  }

  if (cur === -1) {
    throw new Error('current form not found in multiforms');
  }

  let next = cur + jumpSteps;
  if (next >= forms.length) {
    next = forms.length - 1;
  }

  const nextForm = forms[next];

  const data = new FormData(form);
  data.forEach((v, k) => {
    const existing = nextForm.querySelector(`[name="${k}"]`);
    if (existing) {
      existing.value = v;
      return;
    }

    const input = document.createElement('input');
    input.setAttribute('name', k);
    input.setAttribute('type', 'hidden');
    input.setAttribute('value', v);
    nextForm.appendChild(input);
  });

  const height = form.scrollHeight;
  form.style.display = 'none';
  nextForm.style.display = 'block';
  if (nextForm.scrollHeight < height) {
    nextForm.style.height = height + 'px';
  }
};

const displayValidationError = (form, r) => {
  // TODO(improvement): Improve errors.

  if ('name' in r) {
    displayMessage('Por favor, digite um nome válido.');
    form.querySelector('[name="name"]').focus();
    return;
  }

  if ('email' in r) {
    displayMessage('Por favor, digite um e-mail válido.');
    form.querySelector('[name="email"]').focus();
    return;
  }

  if ('phone' in r) {
    displayMessage('Por favor, digite um telefone válido.');
    form.querySelector('[name="phone"]').focus();
    return;
  }

  if ('uf' in r) {
    displayMessage('Por favor, escolha um estado.');
    form.querySelector('[name="uf"]').focus();
    return;
  }

  if ('city' in r) {
    displayMessage('Por favor, escolha uma cidade.');
    form.querySelector('[name="city"]').focus();
    return;
  }

  displayMessage('Erro ao enviar formulário.');
};

const captchaV2ID = 'evag-captcha-checkbox';

const handleSubmit = function (
  form,
  {channel, event, schema, jumpCallback, captchaVal, afterSubmit}
) {
  // Make sure form is visible (captcha could have hidden it).
  form.style.display = 'block';

  // Get channel from event if it's not given.
  if (!channel) {
    const match = form
      .getAttribute('action')
      .match(/channels\/([^/]*)\/submit/);
    if (!match) {
      return;
    }

    event.preventDefault();
    channel = match[1];
  }

  // Fill values.
  const data = new FormData(form);
  let values = {};
  data.forEach((v, k) => {
    values[k] = v;
  });

  // Client-side validation.
  if (schema) {
    try {
      values = schema.validateSync(values);
    } catch (e) {
      console.error(e);
      // TODO(improvement): Display validation errors and return.
    }
  }

  // Use reaptcha v3 by default, but use v2 is captchaVal is given.
  let captchaMode = 'v3';
  if (captchaVal) {
    captchaMode = 'v2';
  }

  // Disable button.
  const button = form.querySelector('button[type="submit"]');
  button.disabled = true;

  const fbEventName = form.dataset.fbEvent;
  const gaEventName = form.dataset.ga4Event;

  const gAdsId = form.dataset.gadsId;
  const gAdsLabel = form.dataset.gadsLabel;

  let gAds = null;
  if (gAdsId && gAdsLabel) {
    gAds = {
      id: !gAdsId.startsWith('AW-') ? `AW-${gAdsId}` : gAdsId,
      label: gAdsLabel,
    }
  }

  submit(channel, values, captchaMode, captchaVal, fbEventName, gaEventName)
    .then(() => {
      if (afterSubmit) {
        afterSubmit(form);
      }

      trackConversion(channel, fbEventName, gaEventName, document.title, gAds);

      const multiforms = form.closest(`[data-evag="multiforms"]`);
      if (multiforms) {
        let jumpSteps = 1;
        if (jumpCallback) {
          jumpSteps = jumpCallback(form);
        }

        jump(form, jumpSteps);
        return;
      }

      clearForm(form);
      displayMessage('Formulário enviado com sucesso!');
    })
    .catch((err) => {
      const res = err.response;
      const errMsg = 'Erro ao enviar formulário.';

      if (!res || !res.status) {
        // Unexpected error.
        displayMessage(errMsg);
        logErr(err, errMsg);
        return;
      }

      if (res.status === 400) {
        if (typeof res.data !== 'object') {
          // Unexpected error.
          displayMessage(errMsg);
          logErr(err, errMsg);
          return;
        }

        // Validation error.
        displayValidationError(form, res.data);
        return;
      }

      if (res.status === 401) {
        // Captcha error. Let's use recaptcha v2.

        const container = document.createElement('div');
        const text = document.createElement('p');
        const captcha = document.createElement('div');

        text.classList.add('evag-captcha-text');
        text.innerHTML =
          'Por favor, confirme que você não é um robô para validar o envio do formulário.';
        captcha.id = captchaV2ID;
        container.appendChild(text);
        container.appendChild(captcha);

        form.parentNode.insertBefore(container, form);
        form.style.display = 'none';

        window.grecaptcha.render(captchaV2ID, {
          callback: (captchaVal) => {
            container.remove();
            handleSubmit(form, {channel, schema, jumpCallback, captchaVal, afterSubmit});
          },
          sitekey: captchaV2Key,
        });

        return;
      }

      displayMessage(errMsg);
      logErr(err, errMsg);
    })
    .finally(() => {
      // Re-enable button.
      button.disabled = false;
    });
};

const handleCEPChange = function (e) {
  const numeric = e.target.value.replace(/[^0-9]/, '');
  if (numeric.length !== 8) {
    return;
  }

  const form = e.target.form;
  const logradouro = form.querySelector('[name="endLogradouro"]');
  const numero = form.querySelector('[name="endNumero"]');
  if (!logradouro || !numero) {
    return;
  }

  logradouro.disabled = true;
  cepData(numeric).then((data) => {
    Object.keys(data)
      .filter((k) => data[k])
      .forEach((k) => {
        const field = form.querySelector(`[name="${k}"]`);
        if (field) {
          field.value = data[k];
        }
      });

    logradouro.disabled = false;
    if (logradouro.value) {
      numero.focus();
    } else {
      logradouro.focus();
    }
  });
};

const handleUFChange = function (e) {
  const city = e.target.form.querySelector('[name="city"]');

  if (!city) {
    return;
  }

  while (city.children.length > 1) {
    city.removeChild(city.lastChild);
  }

  if (!e.target.value) {
    return;
  }

  locations('BR', e.target.value).then((res) => {
    const data = res.map((el) => el.name);
    for (let i = 0; i < data.length; i++) {
      const opt = document.createElement('option');
      opt.setAttribute('value', data[i]);
      opt.innerHTML = data[i];
      city.appendChild(opt);
    }

    handleCityChange({target: city});
  });
};

const handleCityChange = function (e) {
  const uf = e.target.form.querySelector('[name="uf"]');
  const district = e.target.form.querySelector('[name="district"]');

  if (!uf || !district) {
    return;
  }

  while (district.children.length > 1) {
    district.removeChild(district.lastChild);
  }

  if (!e.target.value) {
    return;
  }

  locations('BR', uf.value, e.target.value).then((res) => {
    const data = res.map((el) => el.name);
    for (let i = 0; i < data.length; i++) {
      const opt = document.createElement('option');
      opt.setAttribute('value', data[i]);
      opt.innerHTML = data[i];
      district.appendChild(opt);
    }
  });
};

/**
 * This is an exported function that should be used to setup evag forms. It
 * adds formatting to fields such as CEP and phone; handlers to handle CEP
 * change (load corresponding address), UF change (load cities), city
 * change (load districts), and submit (via XHR).
 *
 * @param {HTMLElement} form - A HTML form that should be handled by EVAG.
 * @param {Object} extra - Extra options.
 * @param {function} extra.jumpCallback - To decide multiforms jump steps.
 * @param {Object} extra.schema - Yup object schema.
 * @param {function} extra.onUFChange - UF change handler.
 * @param {function} extra.onCityChange - City change handler.
 * @param {function} extra.onDistrictChange - District change handler.
 * @param {function} extra.beforeSubmit - Callback to run before submitting.
 * @param {function} extra.afterSubmit - Callback to run after successful submit.
 *
 * @example
 *  // Handle all forms.
 *  document.querySelectorAll('[data-evag="form"]').forEach(setup);
 *
 * @example
 *  // Use yup schema validation.
 *  const form = document.getElementByClassName('nice-form');
 *  const schema = yup.object().shape({
 *    name: yup.string().required(),
 *    email: yup.string().email(),
 *  });
 *  setup(form, {schema});
 *
 * @example
 *  // Use custom handlers.
 *  const form = document.getElementByClassName('nice-form');
 *  const onUFChange = e => {
 *    if (e.target.value === 'SP') {
 *      form.classList.add('show-custom-checkbox');
 *    } else {
 *      form.classList.remove('show-custom-checkbox');
 *    }
 *  };
 *  const onCityChange = e => {
 *    console.log('City changed', e);
 *  };
 *  setup(form, {onUFChange, onCityChange});
 */
export const setup = function (
  form,
  {jumpCallback, schema, onUFChange, onCityChange, onDistrictChange, beforeSubmit, afterSubmit} = {}
) {
  recaptcha.setup();

  // Format phone.
  form.querySelectorAll('[data-evag="phone"]').forEach(
    (element) =>
      new Cleave(element, {
        blocks: [0, 2, 9],
        delimiters: ['(', ') '],
        numericOnly: true,
      })
  );

  // Format dates (dd/mm/yyyy).
  form.querySelectorAll('[data-evag="date"]').forEach(
    (element) =>
      new Cleave(element, {
        blocks: [2, 2, 4],
        delimiter: '/',
        numericOnly: true,
      })
  );

  // Format documents.
  form.querySelectorAll('[data-evag="cpf"]').forEach(
    (element) =>
      new Cleave(element, {
        blocks: [3, 3, 3, 2],
        delimiters: ['.', '.', '-'],
        numericOnly: true,
      })
  );
  form.querySelectorAll('[data-evag="cnpj"]').forEach(
    (element) =>
      new Cleave(element, {
        blocks: [2, 3, 3, 4, 2],
        delimiters: ['.', '.', '/', '-'],
        numericOnly: true,
      })
  );
  form.querySelectorAll('[data-evag="tituloEleitor"]').forEach(
    (element) =>
      new Cleave(element, {
        blocks: [4, 4, 4],
        delimiter: ' ',
        numericOnly: true,
      })
  );

  // Format values.
  form.querySelectorAll('[data-evag="valor"]').forEach(
    (element) =>
      new Cleave(element, {
        numeral: true,
        numeralDecimalMark: ',',
        delimiter: '.',
        prefix: 'R$ ',
      })
  );

  // Format CEP and handle CEP change.
  form.querySelectorAll('[data-evag="cep"], [name="endCEP"]').forEach((element) => {
    // eslint-disable-next-line no-new
    new Cleave(element, {
      blocks: [5, 3],
      delimiter: '-',
      numericOnly: true,
    });

    if (element.getAttribute('name') === 'endCEP') {
      element.addEventListener('input', handleCEPChange);
    }
  });

  // Handle UF change.
  form.querySelectorAll('[name="uf"]').forEach((element) => {
    const listener = (ev) => {
      if (onUFChange) {
        onUFChange(ev, form);
      }
      handleUFChange(ev);
    };

    listener({target: element});
    element.addEventListener('change', listener);
  });

  // Handle city change.
  form.querySelectorAll('[name="city"]').forEach((element) => {
    const listener = (ev) => {
      if (onCityChange) {
        onCityChange(ev, form);
      }
      handleCityChange(ev);
    };

    listener({target: element});
    element.addEventListener('change', listener);
  });

  // Handle district change.
  if (onDistrictChange) {
    form.querySelectorAll('[name="district"]').forEach((element) => {
      onDistrictChange({target: element}, form);
      element.addEventListener('change', (ev) => {
        onDistrictChange(ev, form);
      });
    });
  }

  // Handle submit.
  form.addEventListener('submit', (event) => {
    if (beforeSubmit) {
      beforeSubmit(form);
    }

    handleSubmit(form, {event, jumpCallback, schema, afterSubmit});
  });
};
