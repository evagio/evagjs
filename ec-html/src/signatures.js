import {count} from './core';

const steps = 30;
const time = 900;

const calculateGoal = (number) => {
  if (number < 100) {
    return 100;
  }

  for (let e = 2; ; e++) {
    if (number < Math.pow(10, e)) {
      const p = Math.pow(10, e - 1);
      return Math.floor((number + p) / p) * p;
    }
  }
};

const format = (number) => (
  Intl
    ? new Intl.NumberFormat('pt-BR').format(number)
    : number
);

const grow = function(container, total, color) {
  let done = 0;
  const goal = calculateGoal(total);

  const bar = document.createElement('div');
  bar.classList.add('evag-signatures-bar');
  bar.style.borderColor = color;

  const current = document.createElement('div');
  current.classList.add('evag-signatures-current');
  current.style.background = color;

  const text = document.createElement('div');
  text.classList.add('evag-signatures-text');
  text.style.color = color;

  bar.appendChild(current);
  container.appendChild(bar);
  container.appendChild(text);

  const increase = function() {
    const number = Math.floor(done * total / steps);
    const width = number / goal;

    current.style.width = `${100.0 * width}%`;
    text.innerHTML = `${format(number)} pessoas já assinaram. ` +
      `Ajude-nos a chegar em ${format(goal)}!`;

    if (done === steps) {
      return;
    }

    done++;
    window.setTimeout(increase, time / steps);
  };

  window.setTimeout(increase, 0);
};

/**
 * This is an exported function that should be used to setup evag signatures
 * bar.
 *
 * @param {HTMLElement} container - A HTML container.
 *
 * @example
 *  // Handle all elements with [data-evag="signatures"].
 *  document.querySelectorAll('[data-evag="signatures"]').forEach(setup);
 */
export const setup = function(container) {
  const channelID = container.dataset.channel;
  if (!channelID) {
    throw new Error('no data-channel in signatures container');
  }

  const color = container.dataset.color
    ? container.dataset.color
    : '#000';

  count(channelID)
    .then(res => {
      grow(container, res.data.count, color);
    })
    .catch(e => {
      throw new Error('could not get channel count');
    });
};
