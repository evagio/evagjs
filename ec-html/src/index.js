import './css/index.scss';

import 'regenerator-runtime/runtime';

import * as core from './core';
import * as forms from './forms';
import * as signatures from './signatures';
import * as snackbar from './snackbar';

const ec = {
  core,
  forms,
  signatures,
  snackbar,
};

window.ec = ec;

export default ec;
